@extends('layouts/master')

@section('extra_css')

    <link href="/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>

@stop

@section('extra_js')

    <script src="/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>

@stop


@section('content')

    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Edit <span class="semi-bold">post</span></h4>
                <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
            </div>
            <div class="grid-body no-border"> <br>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  action="/administrator/update_post/{{$post->id}}" method="post" enctype="multipart/form-data">
                    @if(Session::has('success-msg'))
                        <p class="alert alert-success">{{ Session::get('success-msg') }}</p>
                    @endif
                    <div class="form-group">
                        <label class="form-label">Name</label>
                        <span class="help">e.g. "Hello World"</span>
                        <div class="input-with-icon  right">
                            <i class=""></i>
                            <input type="text" name="name" value="{{old('name',$post->name)}}"  id="form1Name" class="form-control">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label">Images</label>
                        <div class="input-with-icon  right">
                            <i class=""></i>
                            <input  type="file" name="image" value="{{old('image',$post->image)}}">
                        </div>
                    </div>

                        


                    <div class="form-group">
                        <label class="form-label">Description</label>
                        <span class="help">e.g. "About Post"</span>
                        <div class="input-with-icon  right">
                            <i class=""></i>
                            <textarea rows="8" cols="100" name="description" >{{old('description',$post->description)}}</textarea>
                        </div>
                    </div>


                    <div class="form-actions">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-danger btn-cons"><i class="icon-ok"></i> Save</button>
                            <button type="button" class="btn btn-white btn-cons">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




@stop