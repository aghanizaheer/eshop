@extends('frontend.master')

@section('content')


<div class="space-custom"></div>

<div class="breadcrumb-area">
		<div class="container">
			<ol class="breadcrumb">
			  <li><a href="#"><i class="fa fa-home"></i></a></li>
			  <li><a href="#">blog</a></li>
			  <li class="active">Data</li>
			</ol>			
		</div>
	</div>
<div class="blog-area">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12 col-sm-12">
					<div class="blog-wrapper blog-main mb-40">
						<div class="blog-img">
							<img alt="" src="{{ asset($post->image)}}">
						</div>
						<div class="blog-info">
							<h3><a href="#">{{ $post->name}}</a></h3>
							<div class="blog-meta blog-large">
								<span>Posted by <b> {{ $post->created_by}} </b></span>
								<span><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> 0 Comments</a></span>
								<span><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> views ({{ $post->views }})</a></span>
							</div>
							
							<p> {{ $post->description }} </p>
						</div>
						
											
						<div class="comments-form mt-40">
							<div class="row">
								<form action="#">
									<div class="col-md-4 mb-30">
										<input type="text" placeholder="Your Name">
									</div>
									<div class="col-md-4 mb-30">
										<input type="email" placeholder="Your Email">
									</div>
									<div class="col-md-4 mb-30">
										<input type="text" placeholder="Your Subject">
									</div>
									<div class="col-md-12">
										<textarea name="message" cols="30" rows="10" placeholder="Comments"></textarea>
										<button class="btn btn-lg mt-30">SUBMIT</button>
									</div>
								</form>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
	</div>

@stop