@extends('frontend.master')

@section('content')




<section>

<div class="slider-container">
        <!-- Slider Image -->
        <div id="mainSlider" class="nivoSlider slider-image">
            <img src="{{asset('img/slider/1.jpg')}}" alt="" title="#htmlcaption1"/>
            <img src="{{asset('img/slider/2.jpg') }}" alt="" title="#htmlcaption2"/>
        </div>
        <!-- Slider Caption 1 -->
        <div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
            <div class="container">
                <div class="slide1-text">
                    <div class="middle-text">
                        <div class="cap-dec cap-1 wow bounceInRight" data-wow-duration="0.9s" data-wow-delay="0s">
                            <h2>A LOOK</h2>
                        </div>  
                        <div class="cap-dec cap-2 wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                            <h2>NEW FASHION</h2>
                        </div>  
                        <div class="cap-text wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.3s">
                            Shop the latest clothing, shoes and handbags from top fashion brands, style icons and celebrities.
                        </div>
                        <div class="cap-readmore wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
                            <a href="#">Shopping</a>
                        </div>  
                    </div>  
                </div>              
            </div>
        </div>
        <!-- Slider Caption 2 -->
        <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
            <div class="container">
                <div class="slide1-text">
                    <div class="middle-text">
                        <div class="cap-dec cap-1 wow bounceInRight" data-wow-duration="0.9s" data-wow-delay="0s">
                            <h2>A LOOK</h2>
                        </div>  
                        <div class="cap-dec cap-2 wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                            <h2>NEW FASHION</h2>
                        </div>  
                        <div class="cap-text wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.3s">
                            Shop the latest clothing, shoes and handbags from top fashion brands, style icons and celebrities.
                        </div>
                        <div class="cap-readmore wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
                            <a href="#">Shopping</a>
                        </div>  
                    </div>  
                </div>                  
            </div>
        </div>
    </div>



    <div class="container">
        <div class="row">


<!-- banner-area start -->
    <div class="banner-area pt-70">
        <div class="container">
            <div class="row">
               <div id="container">
               @foreach($sub_category as $cat)
                    <div class="item banner-style" >
                        <a href="/category/{{ $cat->id }}">
                            <img src="{{asset($cat->image)}}" alt="" />
                            <span class="text"> 
                                <span class="text1">
                                    
                                    <span>{{$cat->name}}</span>
                                    
                                </span> 
                                <span class="text2">shop now</span> 
                            </span>                         
                        </a>
                    </div>
               @endforeach
           </div>


            </div>
        </div>
    </div>
    <!-- banner-area end -->
    <!-- new-arrival-area start -->
    <div class="new-arrival-area pt-100 pb-60">
        <div class="container">
            <div class="row">
               
@if(Session::has('success-msg'))
                        <p class="alert alert-success">{{ Session::get('success-msg') }}<a href="/cart"> Click to view cart </a></p>
                    @endif
                    @if(Session::has('paid-msg'))
                        <p class="alert alert-success">{{ Session::get('paid-msg') }}</p>
                    @endif
            

                <div class="section-title text-center mb-20">
                    <h2>new arrivals</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="product-tab">
               

                        <!-- Nav tabs -->
                        <ul class="custom-tab text-center mb-40">
                            <li class="active"><a href="#home" data-toggle="tab">New Arrivals</a></li>
                            <li><a href="#profile" data-toggle="tab"> Best Seller </a></li>
                            <li><a href="#messages" data-toggle="tab"> Most View</a></li>
                            <li><a href="#settings" data-toggle="tab"> Sale</a></li>
                            <li><a href="#new" data-toggle="tab">  What's New</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="row">
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">
                                    <div class="product-carousel owl-carousel owl-theme">
                
                    @foreach ($products as $product)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                       
                         <div class="product-img" >


                                        <a href="/product/{{$product->id}}">
                            
                             @if (sizeof($product->images) > 0)
<img style="width:268px;height: 350px" src="{{$product->images[0]->image}}" alt="" />
                                    @endif

                            </a>
                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
            <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
            
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$product->id}}">{{ $product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$product->offer_price}}</span>
                                                            <span class="old-price product-price">${{$product->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                                    </div>                                      
                                </div>
                                <div class="tab-pane" id="profile">
                                    <div class="product-carousel owl-carousel owl-theme">
                        
                            @foreach ($bs_products as $BS_Product)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img" >
                                                  
                            

                             @foreach ($images as $image)
  
                    @if($BS_Product->id == $image->product_id)
<a href="/product_details/{{$BS_Product->id}}">
<img style="width:268px;height: 350px" src="{{$image->image}}" alt="" />
                                 </a>
                @endif
                                    @endforeach

                            
                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                            <a href="/product/{{$BS_Product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$BS_Product->id}}">{{ $BS_Product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$BS_Product->offer_price}}</span>
                                                            <span class="old-price product-price">${{$BS_Product->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                                    </div>                              
                                </div>
                                <div class="tab-pane" id="messages">
                                    <div class="product-carousel owl-carousel owl-theme">
                                        
                                          @foreach ($mv_products as $mv)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img">
                                                @foreach ($images_mv as $image)
  
                    @if($mv->id == $image->product_id)
<a href="/product_details/{{$mv->id}}">
<img style="width:268px;height: 350px" src="{{$image->image}}" alt="" />
                                 </a>
                @endif
                                    @endforeach

                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                                                      <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$mv->id}}">{{ $mv->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$mv->offer_price}}</span>
                                                            <span class="old-price product-price">${{$mv->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                                    </div>                              
                                </div>
                                <div class="tab-pane" id="settings">
                                    <div class="product-carousel owl-theme owl-carousel">
                                
                                   @foreach ($products as $product)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img" >
                                                    <a href="/product/{{$product->id}}">
                            
                             @if (sizeof($product->images) > 0)
<img style="width:268px;height: 350px" src="{{$product->images[0]->image}}" alt="" />
                                    @endif

                            </a>
                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                                                      <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$product->id}}">{{ $product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$product->offer_price}}</span>
                                                            <span class="old-price product-price">${{$product->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                                    </div>                              
                                </div>
                                <div class="tab-pane" id="new">
                                    <div class="product-carousel owl-theme owl-carousel">
                                                         @foreach ($products as $product)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img" >
                                                    <a href="/product/{{$product->id}}">
                            
                             @if (sizeof($product->images) > 0)
<img style="width:268px;height: 350px" src="{{$product->images[0]->image}}" alt="" />
                                    @endif

                            </a>
                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                                                      <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$product->id}}">{{ $product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$product->offer_price}}</span>
                                                            <span class="old-price product-price">${{$product->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                                    </div>                              
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
    <!-- new-arrival-area end -->
    <!-- newsletter-area start -->
    <div class="newsletter-area ptb-80 bg-1">
        <div class="container">
            <div id="newsletter_block_left">
                <h4><span>Sign up</span> for newsletter</h4>
                <p>Get exclusive deals you wont find anywhere else straight to your inbox!</p>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-8">
                    <div class="block_content mt-30">
                        <form id="mc-form" class="mc-form form-group">
                            <input id="mc-email" type="email" autocomplete="off" placeholder="Your Email">
                            <button  id="mc-submit" type="submit">Subscribe</button>
                        </form>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts text-centre">
                             <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                             <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                             <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div><!-- mailchimp-alerts end -->
                    </div>                  
                </div>
            </div>
        </div>
    </div>
    <!-- newsletter-area end -->
    <!-- best-sell-area start -->
    <div class="best-sell-area pt-100">
        <div class="container">
            <div class="row">
                <div class="section-title text-center mb-50">
                    <h2>Bestseller Products </h2>
                </div>
            </div>      
            <div class="row">
                <div class="product-carousel">
                    
                            @foreach ($bs_products as $BS_Product)
                                        
                                        <div class="col-md-12">
                                      
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img" >
                                                  
                            

                             @foreach ($images as $image)
  
                    @if($BS_Product->id == $image->product_id)
<a href="/product_details/{{$BS_Product->id}}">
<img style="width:268px;height: 350px" src="{{$image->image}}" alt="" />
                                 </a>
                @endif
                                    @endforeach

                            
                            
                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                            <a href="/product/{{$BS_Product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="/product_details/{{$BS_Product->id}}">{{ $BS_Product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{$BS_Product->offer_price}}</span>
                                                            <span class="old-price product-price">${{$BS_Product->price}}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>

                              @endforeach
                
                </div>                  
            </div>
        </div>
    </div>
    <!-- best-sell-area end -->
    <!-- latest-blog-area start -->
    <div class="latest-blog-area ptb-60">
        <div class="container">
            <div class="row">
                <div class="section-title text-center mb-50">
                    <h2>latest blog</h2>
                </div>
            </div>          
            <div class="row">
                <div class="blog-active">
                
                @foreach($posts as $post)

                    <div class="col-lg-12">
                        <div class="blog-wrapper mb-40">
                            <div class="blog-img">
                                <a href="/post/{{ $post->slug }}"><img src="{{asset($post->image)}}" alt="{{ $post->name }}" /></a>
                            </div>
                            <div class="blog-info">
                                <h3><a href="/post/{{ $post->slug }}"> {{ $post->name }} </a></h3>
                                <div class="blog-meta">
                                    <span class="f-left"> {{ $post->created_at}}</span>
                                    <span class="f-right"><a href="/post/{{ $post->slug }}">Read More </a></span>
                                </div>
                            </div>
                        </div>              
                    </div>
               @endforeach

                </div>  
            </div>
        </div>
    </div>
    <!-- latest-blog-area end -->
    <!-- brand-area start -->
    <div class="brand-area">
        <div class="container">
            <div class="brand-sep ptb-50">
                <div class="row">
                    <div class="brand-active">
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/1.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/2.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/3.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/4.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/5.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/1.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="img/brand/2.jpg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- brand-area end -->
    <!-- service-area start -->
    <div class="service-area pt-70 pb-40 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30">
                        <div class="service-icon">
                            <i class="pe-7s-world"></i>
                        </div>
                        <div class="service-title">
                            <h3>FREE SHIPPING</h3>
                            <p>Free shipping on all UK orders</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30">
                        <div class="service-icon">
                            <i class="pe-7s-refresh"></i>
                        </div>
                        <div class="service-title">
                            <h3>FREE EXCHANGE</h3>
                            <p>30 days return on all items</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30 sm-mrg">
                        <div class="service-icon">
                            <i class="pe-7s-headphones"></i>
                        </div>
                        <div class="service-title">
                            <h3>PREMIUM SUPPORT</h3>
                            <p>We support online 24 hours a day</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30 xs-mrg sm-mrg">
                        <div class="service-icon">
                            <i class="pe-7s-gift"></i>
                        </div>
                        <div class="service-title">
                            <h3>BLACK FRIDAY</h3>
                            <p>Shocking discount on every friday</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service-area end -->

            <!-- @ include('frontend.sidebar')
            
            <div class="col-sm-9 padding-right">
                <div class="features_items">features_items
                    <h2 class="title text-center">All Items</h2>
                    @if(Session::has('success-msg'))
                        <p class="alert alert-success">{{ Session::get('success-msg') }}</p>
                    @endif
                    @if(Session::has('paid-msg'))
                        <p class="alert alert-success">{{ Session::get('paid-msg') }}</p>
                    @endif
            
                    @foreach ($products as $product)
            
                    <div class="col-sm-6" >
                        <div class="product-image-wrapper" >
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    @if (sizeof($product->images) > 0)
                                    <img style="width: 300px;height: 300px" src="{{$product->images[0]->image}}" alt="" />
                                    @endif
                                    <h2>{{$product->offer_price}}$</h2>
                                    <p>{{$product->name}}</p>
                                    <a href="/product/{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
            
            
                </div>features_items
                <div class="pull-right">
                    {!! $products->render() !!}
                </div>
            </div>
                    </div> -->
    </div>
</section>

@stop
