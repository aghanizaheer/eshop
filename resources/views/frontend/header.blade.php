

<header class="header-pos">
        <div class="header-area header-middle">
            <div class="container">
                
                    <div class="col-md-2 col-sm-3 col-xs-12">
                        <div class="logo">
                            <a href="{{ url('/')}}"><img src="{{asset($system->logo) }}" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-9 col-xs-12 text-right xs-center">
                        <div class="main-menu display-inline hidden-sm hidden-xs">
                            <nav>
                                <ul>
                                    <li><a href="{{ url('/')}}">Home</a>   
                                  
                              

            @foreach($pcategory as $category)
          
            <li><a href="#">{{$category->name}}</a>   

            <ul class="submenu">


            @foreach( $sub_category as  $sc )
        
            @if($sc->parent == $category->name)
            
            <li><a href="/category/{{$sc->id}}">{{$sc->name}}</a></li>

            @endif

            @endforeach

            </ul>

            @endforeach


            </li>

                                
             <li><a href="/contact"> contact</a></li>


                                 
                                </ul>
                            </nav>
                        </div>
                        <div class="search-block-top display-inline">
                            <div class="icon-search"></div>
                            <div class="toogle-content">
                         
                    <form action="/" method="post" id="searchbox">
                    <div class="input-group">
                    <input type="text" name="search" placeholder="Search">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <span class="input-group-btn">
                    <button class="button-search"></button>

                    </span>
                    </div>
                    </form>


                            </div> 
                        </div>
                        <div class="shopping-cart ml-20 display-inline">
                            <a href="/cart"><b>shopping cart</b>{{Cart::count() }}</a>
                            <ul>

                    
                            @if(Cart::content()->count() < 1)

                             <li>  Cart Empty. </li>
    
                            @else

                            @foreach(Cart::content() as $cart)
                
                                <li>
                                    <div class="cart-img">
                                    
                                    @foreach($cart->options as $key=>$value)      
                        
                        <img src="{{ asset($value)}}" alt="" style="height: 80px; width: 73px;" />

                                    @endforeach

                                    </div>
                                    <div class="cart-content">
                                        <h3><a href="#"> Quantity: {{$cart->qty}} X</a> </h3>
                                        <span><b>{{ $cart->name }}</b></span>
                                        <span class="cart-price">$ {{ $cart->price }}</span>
                                    </div>
                                    <div class="cart-del">
                                      <a href="/cart_delete/{{$cart->rowid}}">  <i class="fa fa-times-circle"></i> </a>
                                    </div>
                                </li>
                           
                            @endforeach


                            @endif
    
                            

                                <li>
                                  <!--   <div class="shipping"> 
                                        <span class="f-left">Shipping </span>
                                        <span class="f-right cart-price"> $7.00</span>  
                                    </div>
                                                                       --> 
                                 <hr class="shipping-border" />
                                    <div class="shipping">
                                        <span class="f-left"> Total </span>
                                        <span class="f-right cart-price">$ {{Cart::total()}}</span> 
                                    </div>
                                </li>
                                <li class="checkout m-0"><a href="/login">checkout <i class="fa fa-angle-right"></i></a></li>
                            </ul>                           
                        </div>
                        <div class="setting-menu display-inline">
                            <div class="icon-nav current"></div>
                            <ul class="content-nav toogle-content">
                                <li class="currencies-block-top">
                                    <div class="current"><b>Currency : USD</b></div>
                                    <ul>
                                        <li><a href="#">Dollar (USD)</a></li>
                                        <li><a href="#">Pound (GBP)</a></li>
                                    </ul>
                                </li>
                                <li class="currencies-block-top">
                                    <div class="current"><b>English</b></div>
                                    <ul>
                                        <li><a href="#">English</a></li>
                                        <li><a href="#">اللغة العربية</a></li>
                                    </ul>
                                </li>
                                <li class="currencies-block-top">
                                    <div class="current"><b>My Account</b></div>
                                    <ul>
                                    <li><a href="/profile">Profile</a></li>
                                    <li><a href="/account"> Account</a></li>
                                        <!-- <li><a href="#">Checkout</a></li> -->
                                       @if(Auth::check())
                            <li><a href="/logout"> Logout</a></li>
                                @else
                                <li><a href="/login"> Login</a></li>
@endif



                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                
            </div>
        </div>
        <div class="mobile-menu-area visible-sm visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile-menu">
                            <nav id="mobile-menu-active">
                                <ul>
                                    <li><a href="{{ url('/')}}">Home</a>    </li>
                                    <li><a href="shop.html">Men</a></li>
                                    <li><a href="shop.html">Women</a></li>
                                </ul>
                            </nav>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>