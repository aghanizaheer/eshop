@extends('frontend.master')

@section('content')


<div class="space-custom"></div>

<div class="breadcrumb-area">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li>
                <li class="active">Category</li>
            </ol>           
        </div>
    </div>

@if(Session::has('success-msg'))
                        <p class="alert alert-success">{{ Session::get('success-msg') }}<a href="/cart"> Click to view cart </a></p>
                    @endif
                    @if(Session::has('paid-msg'))
                        <p class="alert alert-success">{{ Session::get('paid-msg') }}</p>
                    @endif


<div class="shop-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h2 class="page-heading mt-40">
                        <span class="cat-name"> {{ $cat->name }}</span>
<span class="heading-counter">There are <span id="number"> </span> products.</span>
                    </h2>
                    <div class="shop-page-bar">
                        <div>   
                            <div class="shop-bar">
                                <!-- Nav tabs -->
                                <ul class="shop-tab f-left" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" data-toggle="tab"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
                                    <li role="presentation"><a href="#profile" data-toggle="tab"><i class="fa fa-th-list" aria-hidden="true"></i></a></li>
                                </ul>
                                <div class="selector-field f-left ml-20 hidden-xs">
                                    <form action="#">
                                        <label>Sort by</label>
                                        <select name="select">
                                            <option value="">----</option>
                                            <option value="">Price: Lowest first</option>
                                            <option value="">Price: Highest first</option>
                                            <option value="">Product Name: A to Z</option>
                                            <option value="">Product Name: Z to A</option>
                                            <option value="">In stock</option>
                                            <option value="">Reference: Lowest first</option>
                                            <option value="">Reference: Highest first</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="selector-field f-left ml-30 hidden-xs">
                                    <form action="#">
                                        <label>Show</label>
                                        <select name="select">
                                            <option value="">12</option>
                                            <option value="">13</option>
                                            <option value="">14</option>
                                        </select>
                                    </form>
                                </div>
                              

                            </div>  
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="row">

                                    
                @if(!empty($products))

                    @foreach($products as $product)


                    
                                        <div class="col-md-4 col-sm-6 p_boxes" onclick="location.href='/product_details/{{$product->id}}'">
                                            <div class="product-wrapper mb-40">
                                                <div class="product-img">
                                                    
                                @foreach($product_images as $product_image)

                                    @if($product->id == $product_image->product_id)
                                                    <a href="#"><img src="{{ $product_image->image}}" alt=""></a>

                                    @endif
                                @endforeach

                                                    <span class="new-label">New</span>
                                                    <div class="product-action">
                                                        <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
                                                        <a href="#"><i class="pe-7s-like"></i></a>
                                                        <a href="#"><i class="pe-7s-folder"></i></a>
                                                        <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="pro-title">
                                                        <h3><a href="product-details.html">{{$product->name}}</a></h3>
                                                    </div>
                                                    <div class="price-reviews">
                                                        <div class="price-box">
                                                            <span class="price product-price">${{ $product->offer_price}}</span>
                                                            <span class="old-price product-price">${{ $product->price }}</span>
                                                        </div>
                                                        <div class="pro-rating">
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                            <a href="#"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                    @endforeach           
                @else

                <h3 style="margin:0px 0px 30px 15px;">  No Product Found.</h3>

                    @endif                                      
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">

                    @foreach($products as $product)
                                    <div class="row mb-50" onclick="location.href='/product_details/{{$product->id}}'">
                                        <div class="col-xs-5 col-sm-5 col-md-4">
                                            <div class="product-wrapper">
                                                <div class="product-img">
                                                 @foreach($product_images as $product_image)

                                    @if($product->id == $product_image->product_id)
                                                    <a href="#"><img src="{{ $product_image->image}}" alt=""></a>

                                    @endif
                                @endforeach
                                                    <span class="new-label">New</span>
                                                </div>
                                            </div>                                          
                                        </div>
                                        <div class="col-xs-7 col-sm-7 col-md-8" onclick="location.href='/product_details/{{$product->id}}'">
                                            <div class="product-content product-list">
                                                <div class="pro-title">
                                                    <h3><a href="/product_details/{{$product->id}}">{{ $product->name}} </a></h3>
                                                </div>                                              
                                                <div class="price-reviews">
                                                    <div class="price-box">
                                                        <span class="price product-price">${{$product->offer_price}}</span>
                                                    </div>
                                                    <div class="pro-rating">
                                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                                    </div>
                                                </div>
                                                <p>{{ $product->description }}</p>
                                                <div class="product-action">
                                                    <a class="cart" href="/product/{{$product->id}}"><span>Add to cart</span></a>
                                                    <a href="#"><i class="pe-7s-like"></i></a>
                                                    <a href="#"><i class="pe-7s-folder"></i></a>
                                                    <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                                </div>
                                                
                                            </div>                                  
                                        </div>
                                    </div>
                    @endforeach      

                                </div>
                                <div class="content-sortpagibar">
                                   

                                </div>                          
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop