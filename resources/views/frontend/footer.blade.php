
    <footer class="black-bg">
        <div class="footer-top-area ptb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="footer-widget">
                            <h3 class="footer-title">Contact info</h3>
                            <div class="footer-contact">
                                <ul>
                                    <li><em class="fa fa-map-marker"></em>8901 Marmora Road, Glasgow <span>D04 89 GR, New York</span></li>
                                    <li><em class="fa fa-phone"></em>Telephones: (+1) 866-540-3229 <span>Fax: (+1) 866-540-3229</span></li>
                                    <li><em class="fa fa-envelope-o"></em>Email: {{--$system->email--}}</li>
                                </ul>
                            </div>                          
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="footer-widget">
                            <h3 class="footer-title">My account</h3>
                            <ul class="block-content">
                                <li><a href="#">My orders</a></li>
                                <li><a href="#">My credit slips</a></li>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">My addresses</a></li>
                                <li><a href="#">My personal info</a></li>   
                            </ul>                           
                        </div>                  
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <div class="footer-widget">
                            <h3 class="footer-title">Information</h3>
                            <ul class="block-content">
                                <li><a href="#">Contact us</a></li>
                                <li><a href="#">Discount</a></li>
                                <li><a href="#">Site map</a></li>
                                <li><a href="#">About us</a></li>   
                                <li><a href="#">Custom service</a></li>                                 
                            </ul>                           
                        </div>                  
                    </div>
                    <div class="col-md-4 col-sm-4 footer-sm">
                        <div class="footer-widget">
                            <h3 class="footer-title">OPENING TIME</h3>
                            <div class="footer-time">
                                <p><span class="ft-content"><span class="day">Monday - Friday</span><span class="time">9:00 - 22:00</span></span></p>
                                <p><span class="ft-content"><span class="day">Saturday</span><span class="time">10:00 - 24:00</span></span></p>
                                <p><span class="ft-content"><span class="day">Sunday</span><span class="time">12:00 - 24:00</span></span></p>
                                <p><span class="ft-content"><span class="day">Thursday</span><span class="time">Free Shipping</span></span></p>
                                <p><span class="ft-content"><span class="day">Friday</span><span class="time">sale of 30%</span></span></p>
                            </div>                          
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bootom-area start -->
        <div class="footer-bootom-area ptb-15">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p>Copyright <a href="#">WPJIT</a>. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="payment">
                            <img src="img/payment.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bootom-area end -->
    </footer>
    <!-- footer end -->


<div class="modal fade" id="UpdateModel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content padd">
                <div class="modal-header">
                    <button type="button" class="close DestroyBox" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                </div>
                <div class="modal-body">

@foreach(Cart::content() as $row)
        <div class="wrapUpdateCart">
            <form action="/cart_update/{{$row->rowid}}" method="post">
            <td class="cart_quantity">
            <div class="cart_quantity_button">
            <input class="cart_quantity_input" type="number" name="quantity" value="{{$row->qty}}" autocomplete="off" size="3">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            </div>

            </td>
            <td>
            <button style="margin-top: -5px;"  type="submit" class="btn btn-primary"> Submit </button>
            </td>
            </form>

            
</div>

@endforeach

                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->