@extends('frontend.master')

@section('content')


<div class="space-custom"></div>
    <!-- breadcrumb start -->
    <div class="breadcrumb-area">
        <div class="container">
            <ol class="breadcrumb">
            @foreach($products as $product)
                <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li>
                <li class="active"> {{ $product->name  }}</li>
            @endforeach
            </ol>           
        </div>
    </div>
    <!-- breadcrumb end -->

                            <div class="container">

                            @if(Session::has('success-msg'))
                                <p class="alert alert-success">{{ Session::get('success-msg') }}<a href="/cart"> Click to view cart </a></p>
                            @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </div>

    <!-- shop-area start -->
    <div class="shop-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5">                   
                    <div class="product-zoom">
                        <!-- Tab panes -->
                             @foreach ($products as $product)
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <div class="pro-large-img">
                                    <img src="{{asset($product->images[0]->image ) }}" alt="" />
                                    <a class="popup-link" href="{{asset($product->images[0]->image ) }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile">
                                <div class="pro-large-img">
                                    <img src="{{asset('img/product/2.jpg') }}" alt="" />
                                    <a class="popup-link" href="{{asset('img/product/2.jpg') }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>                          
                            </div>
                            <div class="tab-pane" id="messages">
                                <div class="pro-large-img">
                                    <img src="{{asset($product->images[0]->image ) }}" alt="" />
                                    <a class="popup-link" href="{{asset($product->images[0]->image ) }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>                          
                            </div>
                            <div class="tab-pane" id="settings">
                                <div class="pro-large-img">
                                    <img src="{{asset($product->images[0]->image ) }}" alt="" />
                                    <a class="popup-link" href="{{asset($product->images[0]->image ) }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>                          
                            </div>
                            <div class="tab-pane" id="settings2">
                                <div class="pro-large-img">
                                    <img src="{{asset($product->images[0]->image ) }}" alt="" />
                                    <a class="popup-link" href="{{asset($product->images[0]->image ) }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>                          
                            </div>
                            <div class="tab-pane" id="settings3">
                                <div class="pro-large-img">
                                    <img src="{{asset($product->images[0]->image ) }}" alt="" />
                                    <a class="popup-link" href="{{asset($product->images[0]->image ) }}">View larger <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </div>                          
                            </div>
                        </div>

                        @endforeach
                        <!-- Nav tabs -->
                        <div class="details-tab">
                            <div class="active"><a href="#home" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                            <div><a href="#profile" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                            <div><a href="#messages" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                            <div><a href="#settings" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                            <div><a href="#settings2" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                            <div><a href="#settings3" data-toggle="tab"><img src="{{asset($product->images[0]->image ) }}" alt="" /></a></div>
                        </div>                          
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-7">
                    <div class="product-details">
                        <h2 class="pro-d-title">{{ $product->name}}</h2>
                        <div class="pro-ref">
                            <p>
                                <label>Condition: </label> 
                                <span>New product</span>
                            </p>                            
                        </div>
                        <div class="price-box">
                            <span class="price product-price">${{ $product->price}}</span>
                            <span class="old-price product-price">${{ $product->offer_price}}</span>
                        </div>
                        <div class="short-desc">
                            <p>{{ $product->description}}</p>
                        </div>
                        <div class="box-quantity">
                            
                          <form action="/product_details/{{$product->id}}" method="POST" >

                                <label>Quantity</label> 
                                <input type="number" name="quantity" value="1"/>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <button type="submit"> add to cart</button>
                            </form>


                        </div>
                        <div class="usefull_link_block">
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>Send to a friend</a></li>
                                <li><a href="#"><i class="fa fa-print"></i>Print</a></li>
                                <li><a href="#"><i class="fa fa-heart-o"></i> Add to wishlist</a></li>
                            </ul>
                        </div>
                    
                        <div class="share-icon">
                            <a class="twitter" href="#"><i class="fa fa-facebook"></i>  facebook</a>
                            <a class="facebook" href="#"><i class="fa fa-twitter"></i>  twitter</a>
                            <a class="google" href="#"><i class="fa fa-google-plus"></i>  linkedin</a>
                            <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>  facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shop-area end -->
    <!-- pro-info-area start -->
    <div class="pro-info-area ptb-80">
        <div class="container">
            <div class="pro-info-box">
                <!-- Nav tabs -->
                <ul class="pro-info-tab rm10" role="tablist">
                   
                    <li class="active"><a href="#messages3" data-toggle="tab">Reviews</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="messages3">
                        <div class="">
                            
                    
                                        @foreach($reviews as $review)
                                        <div class="list-group">
                                            <a class="list-group-item ">
                                                <h4 class="list-group-item-heading headingName"> Name: {{$review->name}}</h4>
                                                <p class="list-group-item-text detailreview ">  {{$review->review}}</p>
                                            </a>
                                        </div>
                                        @endforeach
                                        <p><b>Write Your Review</b></p>
                                        @if(Session::has('success-msg-review'))
                                            <p class="alert alert-success">{{ Session::get('success-msg-review') }}</p>
                                        @endif

                                        <form action="/review/{{$product->id}}" method="post">
                                        <span>
                                            <input type="text" name="name" placeholder="Your Name"/>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="email" name="email" placeholder="Email Address"/>
                                        </span>
                                            <textarea name="review" ></textarea>
                                            <button type="submit" class="btn btn-default pull-right">
                                                Submit
                                            </button>
                                        </form>
                    

                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
    <!-- pro-info-area end -->
   
    <!-- best-sell-area start -->
    <div class="best-sell-area ptb-60">
        <div class="container">
            <div class="row">
                <div class="section-title text-center mb-50">
                    <h2>12 other products in the same category: </h2>
                </div>
            </div>      
            <div class="row">
                <div class="product-carousel owl-carousel owl-theme">

                @foreach($related as $product)
                  
                    @foreach($products  as $pro)

                        @if($product->id == $pro->id)

                            <?php continue; ?>

                        @endif

                    <div class="col-md-12" onclick="location.href='/product_details/{{$product->id}}'">
                        <div class="product-wrapper mb-40 mrg-nn-xs">
                            <div class="product-img">
                    @foreach($product_image as $image)


                        @if($product->id == $image->product_id)


                    
                                <a href="#"><img src="{{asset($image->image ) }}" alt="" /></a>
                            
                        @endif

                    @endforeach
                                <span class="new-label">New</span>
                                <div class="product-action">
                     <a href="/product/{{$product->id}}"><i class="pe-7s-cart"></i></a>
                                    <a href="#"><i class="pe-7s-like"></i></a>
                                    <a href="#"><i class="pe-7s-folder"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#productModal"><i class="pe-7s-look"></i></a>
                                </div>
                            </div>
                            <div class="product-content">
                                <div class="pro-title">
                                    <h3><a href="product-details.html">{{ $product->name }}</a></h3>
                                </div>
                                <div class="price-reviews">
                                    <div class="price-box">
                                        <span class="price product-price">${{ $product->offer_price}}</span>
                                        <span class="old-price product-price">${{ $product->price}}</span>
                                    </div>
                                    <div class="pro-rating">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               

                    @endforeach                   

                @endforeach                   
                 
                </div>                      
            </div>
        </div>
    </div>
    <!-- best-sell-area end --> 
    <!-- brand-area start -->
    <div class="brand-area">
        <div class="container">
            <div class="brand-sep ptb-50">
                <div class="row">
                    <div class="brand-active">
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/1.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/2.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/3.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/4.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/5.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/1.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="single-brand">
                                <a href="#"><img src="{{asset('img/brand/2.jpg') }}" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- brand-area end -->
    <!-- service-area start -->
    <div class="service-area pt-70 pb-40 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30">
                        <div class="service-icon">
                            <i class="pe-7s-world"></i>
                        </div>
                        <div class="service-title">
                            <h3>FREE SHIPPING</h3>
                            <p>Free shipping on all UK orders</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30">
                        <div class="service-icon">
                            <i class="pe-7s-refresh"></i>
                        </div>
                        <div class="service-title">
                            <h3>FREE EXCHANGE</h3>
                            <p>30 days return on all items</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30 sm-mrg">
                        <div class="service-icon">
                            <i class="pe-7s-headphones"></i>
                        </div>
                        <div class="service-title">
                            <h3>PREMIUM SUPPORT</h3>
                            <p>We support online 24 hours a day</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-service mb-30 xs-mrg sm-mrg">
                        <div class="service-icon">
                            <i class="pe-7s-gift"></i>
                        </div>
                        <div class="service-title">
                            <h3>BLACK FRIDAY</h3>
                            <p>Shocking discount on every friday</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service-area end -->
@stop

<!-- @ extends('frontend.master')

@section('extra_js')

    <script src="/assets_frontend/js/jquery.magnify.js"></script>

    <script>
        $(document).ready(function() {
            $('.clicked_here').click(function(){
                console.log($(this));
                var img = $(this).attr('src');
                $('#show_here').attr('src',img);
                $('#show_here').attr('data-magnify-src',img);
                $('#show_here').magnify();
            });
            $('#show_here').magnify();
        });
    </script>

    @stop

@section('extra_css')

    <link rel="stylesheet" href="/assets_frontend/css/magnify.css">

@stop

@section('content')

    <section>
        <div class="container">
            <div class="row">
                @include('frontend.sidebar')


                <div class="col-sm-9 padding-right">
                    @foreach ($products as $product)

                    <div class="product-details">product-details
                        <div class="col-sm-5">

                            <div class="view-product">

                                <img id="show_here" src="{{$product->images[0]->image}}" data-magnify-src="{{$product->images[0]->image}}" />

                            </div>

                            <div id="similar-product" class="carousel slide" data-ride="carousel">

                                Wrapper for slides
                                <div class="carousel-inner">
                                    <div class="item active">
                                        @foreach($product->images as $product->image)
                                        <img class="clicked_here" style="width: 60px"src="{{$product->image->image}}" alt="">
                                      @endforeach
                                    </div>


                                </div>

                                Controls
                                <a class="left item-control" href="#similar-product" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right item-control" href="#similar-product" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>

                        </div>

                        <div class="col-sm-7">
                            @if(Session::has('success-msg'))
                                <p class="alert alert-success">{{ Session::get('success-msg') }}</p>
                            @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            <div class="product-information">/product-information
                                <img src="/assets_frontend/images/product-details/new.jpg') }}" class="newarrival" alt="" />
                                <h2>{{$product->name}}</h2>
                                <span>

                                    <span>{{$product->price}}$</span>

                                    <form action="/product_details/{{$product->id}}" method="POST" >
                                        <div class="col-lg-12">
                                            <label style="margin-left: 50px;">Quantity:</label>
                                            <input type="number" name="quantity" value="1"/>
                                        </div>

                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button  type="submit"  class="btn btn-default cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        Add to cart
                                    </button>
                                    </form>
                                </span>


                                <a href=""><img src="/assets_frontend/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                            </div>/product-information
                        </div>
                    </div>/product-details
@endforeach
                        <div class="category-tab shop-details-tab">category-tab
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#details" data-toggle="tab">Details</a></li>

                                    <li ><a href="#reviews" data-toggle="tab">Reviews</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="details" >

<                           p>{{$product->description}}</p>
                                </div>



                                <div class="tab-pane " id="reviews" >
                                    <div class="col-sm-12">
                                        <p><b>Reviews</b></p>
                                        @foreach($reviews as $review)
                                        <div class="list-group">
                                            <a class="list-group-item ">
                                                <h4 class="list-group-item-heading">{{$review->name}}</h4>
                                                <p class="list-group-item-text">{{$review->review}}</p>
                                            </a>
                                        </div>
                                        @endforeach
                                        <p><b>Write Your Review</b></p>
                                        @if(Session::has('success-msg-review'))
                                            <p class="alert alert-success">{{ Session::get('success-msg-review') }}</p>
                                        @endif

                                        <form action="/review/{{$product->id}}" method="post">
                                        <span>
                                            <input type="text" name="name" placeholder="Your Name"/>
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="email" name="email" placeholder="Email Address"/>
                                        </span>
                                            <textarea name="review" ></textarea>
                                            <button type="submit" class="btn btn-default pull-right">
                                                Submit
                                            </button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>/category-tab


                </div>

            </div>
        </div>
    </section>

@stop
 -->