@extends('frontend.master')

@section('content')

<div class="space-custom"></div>

<div class="breadcrumb-area">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li>
                <li class="active">Contact</li>
            </ol>           
        </div>
    </div>


    <div id="contact-page" class="container">
        <div class="bg">
            <div class="row">
                <div class="col-sm-8">
                    <div class="contact-form">
                        <h2 class="title text-center">Get In Touch</h2> <br/><br/>
                        <div class="status alert alert-success" style="display: none"></div>
                        @if(Session::has('success-msg'))
                            <p class="alert alert-success">{{ Session::get('success-msg') }}</p>
                        @endif
                        <form id="main-contact-form" class="contact-form row" name="contact-form" action="/contact" method="post">
                            <div class="form-group col-md-6">
                                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="subject" class="form-control" required="required" placeholder="Subject">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="submit" class="btn btn-primary pull-left" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-info">
                        <h2 class="title text-center">Contact Info</h2><br/><br/>
                        <address>
                            <p>E-Shop</p>
                            <p>935 W. Webster Ave New Streets Chicago, IL 60614, NY</p>
                            <p>Newyork USA</p>
                            <p>Mobile: +76765756</p>
                            <p>Fax: 45754747</p>
                            <p>Email: info@e-shop.com</p>
                        </address>
                        <div class="social-networks">
                            <h2 class="title text-left">Social Networking</h2>
                            <ul class="list-unstyled list-inline">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#contact-page-->

<br/><br/><br/>
@stop