@extends('frontend.master')

@section('content')


<div class="space-custom"></div>

<div class="breadcrumb-area">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Cart</li>
            </ol>           
        </div>
    </div>

    <div class="cart-main-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                          
                        <div class="table-content table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product-thumbnail">Image</th>
                                        <th class="product-name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product-quantity">Quantity</th>
                                        <th class="product-subtotal">Total</th>
                                        <th class="product-remove">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                             
                             @if(Session::has('paid-msg'))
                        <p class="alert alert-success">{{ Session::get('paid-msg') }}</p>
                    @endif
                    @if(Session::has('success-msg'))
                        <p class="alert alert-info">{{ Session::get('success-msg') }}</p>
                    @endif



  @foreach(Cart::content() as $row)

    
    <tr>
        <td class="product-thumbnail"><a href="#">
       

        @foreach($row->options as $key=>$value)      

        <img src="{{ asset($value)}}" alt=""></a></td>
        
        @endforeach      

        <td class="product-name"><a href="#">{{ $row->name}}</a></td>
        <td class="product-price"><span class="amount">${{ $row->price}}</span></td>
        <td class="product-quantity"><input type="number" value="{{ $row->qty }}"></td>
        <td class="product-subtotal">${{$row->subtotal}}</td>
        <td class="product-remove"><a href="/cart_delete/{{$row->rowid}}"><i class="fa fa-times"></i></a></td>
    </tr>
             


@endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-7 col-xs-12">
                                <div class="buttons-cart">
                                    <input type="button" value="Update Cart" data-toggle="modal" data-target="#UpdateModel">
                                    <a href="{{ url('/') }}">Continue Shopping</a>
                                </div>
                @if(Auth::check())
                                <div class="coupon">
                                    <h3>Coupon</h3>
                                    <p>Enter your coupon code if you have one.</p>
                                 
        <form action="/coupons" method="post">
        <div class="input-group col-md-12">
        <input type="text" name="code"  placeholder="Coupon Code" >
        <button  class="btn btn-default" type="submit">Apply</button>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <span class="input-group-btn">
        </span>
        </div>
        </form>


                                </div>
                            @endif
                            </div>
                            <div class="col-md-6 col-sm-5 col-xs-12">
                                <div class="cart_totals">
                                    <h2>Cart Totals</h2>
                                    <table>
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>Subtotal</th>
                                                <td><span class="amount">${{ Cart::total() }}</span></td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td>
                                                    <strong><span class="amount">${{ Cart::total() }}</span></strong>
                                                </td>
                                            </tr>                                           
                                        </tbody>
                                    </table>
                                    </div>

                                    <div class="clearfix PayMethods">

                        

            @if(Auth::check())
                        <a class="btn btn-default check_out" href="/payment">Pay with Paypal</a>
                        <a class="btn btn-default check_out" href="/cod">Cash on delivery</a>
                        <form action="/stripe" method="POST" style="float: left;display: inline;margin-right:10px">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_test_FE92z9o3yzB4gQbS6agPBlfW"
                                    data-amount="{{$cart_total*100-(Session::get('coupon_value')*100)}}"
                                    data-name="Eshop"
                                    data-description="You will be charged {{$cart_total-(Session::get('coupon_value'))}}$"
                                    >
                            </script>

                        </form>

                        @endif


                         @if(!Auth::check())

                         <div class="wc-proceed-to-checkout">
                        <a href="/login">Proceed to Checkout</a>
                                    </div>

                                    @endif
<br/><br/><br/>
                                </div>
                            </div>
                        </div>
             
                </div>
            </div>
        </div>
    </div>



@stop
