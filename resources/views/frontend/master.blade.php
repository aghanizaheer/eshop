<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{!empty($seo)?$seo->description:''}}">
    <meta name="keywords" content="{{!empty($seo)?$seo->keyword:''}}">



    <title>{{!empty($system)?$system->title:''}}</title>
     <link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}">
    <link href="/assets_frontend/css/font-awesome.min.css" rel="stylesheet">
<!--     <link href="/assets_frontend/css/prettyPhoto.css" rel="stylesheet">
<link href="/assets_frontend/css/price-range.css" rel="stylesheet">
<link href="/assets_frontend/css/animate.css" rel="stylesheet">
<link href="/assets_frontend/css/main.css" rel="stylesheet">
<link href="/assets_frontend/css/responsive.css" rel="stylesheet"> -->
    @yield('extra_css')

    <!--[if lt IE 9]>
    <script src="/assets_frontend/js/html5shiv.js"></script>
    <script src="/assets_frontend/js/respond.min.js"></script>
    <![endif]-->
    <link rel="/assets_frontend/shortcut icon" href="{{asset('assets_frontend/images/ico/favicon.ico' )}}">
    
    
    <!-- MANIA -->
<link rel="stylesheet" href="{{asset('css/shortcode/header.css') }}">
     <link rel="stylesheet" href="{{asset('css/core.css') }}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{asset('css/shortcode/shortcodes.css') }}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{asset('style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{asset('css/responsive.css') }}">
    <!-- User style -->
    <link rel="stylesheet" href="{{asset('css/custom.css') }}">
    
    <!-- Modernizr JS -->
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>




    {!!!empty($seo)?$seo->google_analytics:''!!}

</head><!--/head-->

<body>

@include('frontend.header')


@yield('content')

@include('frontend.footer')


<script src="/assets_frontend/js/jquery.js"></script>
@yield('extra_js')
<script src="/assets_frontend/js/price-range.js"></script>
<script src="/assets_frontend/js/jquery.scrollUp.min.js"></script>
<script src="/assets_frontend/js/bootstrap.min.js"></script>
<script src="/assets_frontend/js/jquery.prettyPhoto.js"></script>
<script src="/assets_frontend/js/main.js"></script>

<!-- MANIA -->

 
    
    <!-- jquery latest version -->
    <script src="{{asset('js/vendor/jquery-1.12.0.min.js') }}"></script>
    <!-- Bootstrap framework js -->
    <script src="{{asset('js/bootstrap.min.js') }}"></script>
    <!-- ajax-mail js -->
    <script src="{{asset('js/ajax-mail.js') }}"></script>
    <!-- owl.carousel js -->
    <script src="{{asset('js/owl.carousel.min.js') }}"></script>
    <!-- jquery.nivo.slider js -->
    <script src="{{asset('js/jquery.nivo.slider.pack.js') }}"></script>
    <!-- All js plugins included in this file. -->
    <script src="{{asset('js/plugins.js') }}"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="{{asset('js/main.js') }}"></script>


<!-- Counting Products In Category -->

<script type="text/javascript">
    
    jQuery(document).ready(function() {

    var le = $('.p_boxes').length;
    $('#number').text(le);


    });
    
</script>



</body>
</html>