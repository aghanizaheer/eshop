<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Post;
use Session;
use Auth;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('posts')->with('posts' , Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());

        $this->validate($request,[

            'name' => 'required',
            'image' => 'required',
            'description' => 'required',

        ]);

        //$user = Auth::user()->name;

        $post = $request->image;
        $new_post_image = time().$post->getClientOriginalName();
        $post->move('media/posts/',$new_post_image);


  

        Post::create([
            'name' => $request->name,
            'image' => 'media/posts/'.$new_post_image,
            'description' =>  $request->description,
            'slug' => str_slug($request->name),
            'created_by' => $request->user,
            'comment_count' => 0,
            'views' => 0,
        ]);



        Session::flash('success-msg' , 'Post Created Successfully');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //dd($slug);

        $post = Post::where('slug' , $slug)->first();

        // Post views Counter
        $post::where('slug', $post->slug)->increment('views');

        


        $pcategory = DB::table('parent_categories')->get();
        $sub_category  = DB::table('sub_categories')->get();
        

        return view('single')->with('post' , $post) 
                             ->with('pcategory' , $pcategory)
                             ->with('sub_category' , $sub_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('edit_post')->with('post' , $post);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //  dd($request->all());  

        $post = Post::find($id);  

        if($request->hasFile('image')){

            $image = $request->image;
            $new_post_image = time().$image->getClientOriginalName();
            $image->move('media/posts',$new_post_image);
            $post->image = 'media/posts/'.$new_post_image;

        }

        $post->name = $request->name;
        $post->description = $request->description;
        $post->slug = str_slug($request->name);

        $post->save();

        Session::flash('success-msg' , 'Post Updated Successfully.');

        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //dd($id);

        $post = Post::find($id);

        if($post->image){
            unlink($post->image);
        }


        $post->delete();

        Session::flash('success-msg' , 'Post Deleted Successfully');
        return redirect()->back();


    }
}
