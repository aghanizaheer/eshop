<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Input;
use Hash;
use Validator;
use Auth;


class SubCategoryController extends BaseController
{

    public function dropDown(){

       // dd('zaheer');
        if(Auth::user()->role=='Administrator' || Auth::user()->role=='Manager' )
        {

        $categories = DB::table('parent_categories')->get();
        //dd($categories);
        return view('/create_subcategory', ['categories' => $categories]);
        }
        else
        {

            return redirect('/administrator/orders');


        }


    }


    public function store(Request $request)
    {
      //  dd($request);

        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            'description' => 'required',
            'parent' => 'required'

        ]);

       

        if ($validator->fails()) {
            return redirect('/administrator/create_subcategory')
                ->withErrors($validator)
                ->withInput();
        }

        
       
        // Getting Featured Image from the request
        $image = Input::file('image');

        //dd($image);
        // Give new name to image which restrict to conflict in names
        $image_new_name = time().$image->getClientOriginalName();

        //Directory where we are moving
        $image->move('media/category' , $image_new_name);


        


        $subcategory_name = Input::get('name');
        $subcategory_description = Input::get('description');
        $subcategory_parent = Input::get('parent');
        $subcategory_image = $image_new_name;

        DB::table('sub_categories')->insert(
            ['name' => $subcategory_name, 'description' => $subcategory_description,'parent'=>$subcategory_parent , 'image' => 'media/category/'.$subcategory_image]
        );

        $subcategories = DB::table('sub_categories')->get();
        $categories = DB::table('parent_categories')->get();

        \Session::flash('success-msg', 'Successfully Added');

        return view('/create_subcategory', ['subcategories' => $subcategories],['categories'=>$categories]);

    }

    public function show()
    {
        if(Auth::user()->role=='Administrator' || Auth::user()->role=='Manager' ) {

            $subcategories = DB::table('sub_categories')->get();

            return view('/subcategories', ['subcategories' => $subcategories]);
        }
        else
            {

                return redirect('/administrator/orders');


            }


    }

    public function edit($id)
    {
        if(Auth::user()->role=='Administrator' || Auth::user()->role=='Manager' )
        {
        $subcategories = DB::table('sub_categories')->where('id', '=', $id)->first();
        $categories = DB::table('parent_categories')->get();


        return view('edit_subcategory', ['subcategories' => $subcategories],['categories'=>$categories]);
        }
        else
        {

            return redirect('/administrator/orders');


        }


    }

    public function update($id)
    {

        $validator = Validator::make(Input::all(), [
            'name' => 'required',
            'description' => 'required',
            'parent' => 'required',
    //        'image' => 'required',



        ]);

        if ($validator->fails()) {
            return redirect('/administrator/edit_subcategory/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        // Getting Featured Image from the request
        if(!empty(Input::file('image'))){

        $image = Input::file('image');

        //dd($image);
        // Give new name to image which restrict to conflict in names
        $image_new_name = time().$image->getClientOriginalName();

        //Directory where we are moving
        $image->move('media/category' , $image_new_name);


        $subcategory_name = Input::get('name');
        $subcategory_description = Input::get('description');
        $subcategory_parent = Input::get('parent');
        $subcategory_image = $image_new_name;

        DB::table('sub_categories')->where('id', $id)->update(
            ['name' => $subcategory_name, 'description' => $subcategory_description, 'image' => 'media/category/'.$subcategory_image , 'parent'=>$subcategory_parent]
        );
        $categories = DB::table('parent_categories')->get();
        $subcategories = DB::table('sub_categories')->where('id', '=', $id)->first();
        \Session::flash('success-msg', 'Successfully Edited');
        return view('edit_subcategory', ['subcategories' => $subcategories],['categories'=>$categories]);
    }

    else{

        $subcategory_name = Input::get('name');
        $subcategory_description = Input::get('description');
        $subcategory_parent = Input::get('parent');
       // $subcategory_image = $image_new_name;

        DB::table('sub_categories')->where('id', $id)->update(
            ['name' => $subcategory_name, 'description' => $subcategory_description,  'parent'=>$subcategory_parent]
        );
        $categories = DB::table('parent_categories')->get();
        $subcategories = DB::table('sub_categories')->where('id', '=', $id)->first();
        \Session::flash('success-msg', 'Successfully Edited');
        return view('edit_subcategory', ['subcategories' => $subcategories],['categories'=>$categories]);
    


    }





    }
    public function delete($id)
    {

        if(Auth::user()->role=='Administrator' || Auth::user()->role=='Manager' )
        {
       
        $query = DB::table('sub_categories')->where('id', '=', $id);
        $image = $query->first();

      //  dd($sub_categories);

        if(file_exists($image->image)){
            unlink($image->image);
        }

        $query->delete();


        $subcategories = DB::table('sub_categories')->get();

        return view('/subcategories', ['subcategories' => $subcategories]);

        }
        else
        {

            return redirect('/administrator/orders');


        }


    }
}

