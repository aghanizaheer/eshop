<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Input;
use Hash;
use Validator;
use Gloudemans\Shoppingcart\Facades\Cart;


class CartController extends BaseController
{

    public function show()
    {
        //dd($id)
     
        if(Cart::content()->count() == 0){

            return redirect()->back();
        }


        $system = DB::table('system')->first();

        $cart = Cart::content();

        if (sizeof($cart) == 0) {
            \Session::forget('coupon_value');
            \Session::flash('success-msg', 'Cart Is Empty');
        }

        $cart_total = Cart::total();
        
        $pcategory = DB::table('parent_categories')->get();
        $scategory = DB::table('sub_categories')->get();

        return view('frontend.cart', ['system' => $system, 'cart' => $cart, 'cart_total' => $cart_total , 'pcategory' => $pcategory , 'sub_category' => $scategory ]);


    }


}

