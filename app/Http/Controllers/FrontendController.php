<?php

namespace App\Http\Controllers;

use DB;
use Input;
use Validator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Session;

class FrontendController extends FrontEndBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {

            
        $query = DB::table('products')->where('status', 'Active')
                                      ->orderBy('id' ,'desc');

        if (Input::has('filter_subcategory', 'filter_category')) {
            $query->where('subcategory', Input::get('filter_subcategory'))->where('category', Input::get('filter_category'));
        }

        $products = $query->paginate(10);

        foreach ($products as $product) {
            $product_images = DB::table('product_images')
                ->where('product_id', $product->id)
                ->get();

            $product->images = $product_images;
        }

        $this->data['products'] = $products;

        // ORDER BY SALE

        $best_sell = DB::table('products')->orderBy('bs', 'DESC')->get();
        // Categories

        //dd($bs_query);

        foreach ($best_sell as $best_sell_image) {
           // dd($bs_query_product);

            $images = DB::table('product_images')
               // ->where('product_id', $best_sell_image->id)
                ->get();
                //dd($images);
            $best_sell_image->images = $images;
        }

        $this->data['bs_products'] = $best_sell;
        $this->data['images'] = $images;

           // ORDER BY MOST VIEW

        $mv_sell = DB::table('products')->orderBy('mv', 'DESC')->get();
        

        foreach ($mv_sell as $most_view_image) {
           // dd($bs_query_product);

            $images_mv = DB::table('product_images')
               // ->where('product_id', $best_sell_image->id)
                ->get();
                //dd($images);
            $most_view_image->images = $images_mv;
        }

        $this->data['mv_products'] = $mv_sell;
        $this->data['images_mv'] = $images_mv;





        $category = DB::table('parent_categories')->get();
        $this->data['pcategory'] = $category;   

        //dd($category);         

        $sub_category = DB::table('sub_categories')->get();
        $this->data['sub_category'] = $sub_category;       

        // Displaying Posts

         $posts = DB::table('posts')->get();
        $this->data['posts'] = $posts;       


      //  $post = Post::all()
//return view('frontend.index')->with('posts' , $post);

        $system = DB::table('system')->first();
       

        return view('frontend.index', $this->data);


    }

    public function priceFilter()
    {


        $max_price = input::get('max');
        $min_price = input::get('min');

        $query = DB::table('products')->where('status', 'Active')->whereBetween('offer_price', [$max_price, $min_price]);

        if (Input::has('filter_subcategory', 'filter_category')) {
            $query->where('subcategory', Input::get('filter_subcategory'))->where('category', Input::get('filter_category'));

        }

        $products = $query->paginate(10);


        foreach ($products as $product) {
            $product_images = DB::table('product_images')
                ->where('product_id', $product->id)
                ->get();

            $product->images = $product_images;
        }

        $this->data['products'] = $products;

        return view('frontend.index', $this->data);

    }

    public function cart($id)
    {
        
        $products = DB::table('products')
            ->where('id', $id)
            ->first();
        $quantity = input::get('quantity');
        $product_name = $products->name;
        $product_price = $products->offer_price;


        if (sizeof($quantity) > 0 && $quantity != 0) {

            $product_image = DB::table('product_images')
            ->where('product_id', $id)
            ->first();
             $image[] = $product_image->image;

            Cart::add($id, $product_name, $quantity, $product_price,$image);

        } else {
            $product_image = DB::table('product_images')
            ->where('product_id', $id)
            ->first();
             $image[] = $product_image->image;
             Cart::add($id, $product_name, 1, $product_price , $image);

        }

        Session::flash('success-msg', 'Successfully Added to Cart');

        return redirect('cart');

    }

    public function search()
    {
        $q = Input::get('search');

        $searchTerms = explode(' ', $q);

        $query = DB::table('products')->where('status', 'Active');

        if (Input::has('filter_subcategory')) {
            $query->where('subcategory', Input::get('filter_subcategory'));
        }

        foreach ($searchTerms as $term) {
            $query->where('name', 'LIKE', '%' . $term . '%');
        }

        $products = $query->paginate(10);
        foreach ($products as $product) {
            $product_images = DB::table('product_images')
                ->where('product_id', $product->id)
                ->get();

            $product->images = $product_images;
        }

        $this->data['products'] = $products;

     $sub_category = DB::table('sub_categories')->get();
        $this->data['sub_category'] = $sub_category;       

        // ORDER BY SALE

        $best_sell = DB::table('products')->orderBy('bs', 'DESC')->get();
        // Categories

        //dd($bs_query);

        foreach ($best_sell as $best_sell_image) {
           // dd($bs_query_product);

            $images = DB::table('product_images')
               // ->where('product_id', $best_sell_image->id)
                ->get();
                //dd($images);
            $best_sell_image->images = $images;
        }

        $this->data['bs_products'] = $best_sell;
                $this->data['images'] = $images;


        // ORDER BY MOST VIEW

        $mv_sell = DB::table('products')->orderBy('mv', 'DESC')->get();
        

        foreach ($mv_sell as $most_view_image) {
           // dd($bs_query_product);

            $images_mv = DB::table('product_images')
               // ->where('product_id', $best_sell_image->id)
                ->get();
                //dd($images);
            $most_view_image->images = $images_mv;
        }

        $this->data['mv_products'] = $mv_sell;
        $this->data['images_mv'] = $images_mv;

        // Displaying Posts

         $posts = DB::table('posts')->get();
        $this->data['posts'] = $posts;       


   $category = DB::table('parent_categories')->get();
        $this->data['pcategory'] = $category;   

        $scategory = DB::table('sub_categories')->get();
        $this->data['scategory'] = $scategory;   
     

        return view('frontend.index', $this->data);
    }

    public function update($id)
    {
        //dd($id);
        $rowId = $id;
        $quantity = input::get('quantity');
        Cart::update($rowId, $quantity);
        Session::flash('success-msg', 'Successfully Updated');
        return redirect()->back();
    }

    public function delete($id)
    {
        $rowId = $id;
        Cart::remove($rowId);
        \Session::flash('success-msg', 'Successfully Removed');
        return redirect()->back();
    }


    public function showDetails($id)
    {
        //dd($id);
        $products = DB::table('products')->where('id', $id)->get();
        $reviews = DB::table('reviews')->where('product_id', $id)->get();
            

        foreach ($products as $product) {
            $product_images = DB::table('product_images')
                ->where('product_id', $product->id)
                ->get();

            $product->images = $product_images;

            //dd($product->subcategory);
         $category = DB::table('sub_categories')
                ->where('name', $product->subcategory)
                ->first();
        
         $related = DB::table('products')->where('subcategory', $category->name)->get();

         

        $product_image = DB::table('product_images')->get();

        $category = DB::table('parent_categories')->get();
        $this->data['pcategory'] = $category;   

        $scategory = DB::table('sub_categories')->get();
        $this->data['scategory'] = $scategory;   
        
      //  $sub_category  = DB::table('sub_categories')->get();

        $products_mv = DB::table('products')->where('id',$product->id)->first();

            if($products_mv->id == $product->id){

            DB::table('products')->where('id', $product->id)->increment('mv');


            }



        }

        $this->data['products'] = $products;
        $this->data['reviews'] = $reviews;
        $this->data['related'] = $related;
        $this->data['product_image'] = $product_image;
        $this->data['p_category'] = $category;
        $this->data['sub_category'] = $scategory;

        return view('frontend.product_details', $this->data);
    }

    public function review($id)
    {

        $name = input::get('name');
        $email = input::get('email');
        $review = input::get('review');

        $v = Validator::make(['name' => $name, 'email' => $email, 'review' => $review], ['name' => 'required', 'email' => 'required|email', 'review' => 'required']);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput(Input::all());
        }
        $product_id = $id;

        DB::table('reviews')->insert(
            ['product_id' => $product_id, 'name' => $name, 'email' => $email, 'review' => $review]
        );

        Session::flash('success-msg-review', 'Successfully Added');

        return redirect()->back();

    }

    public function category($id){
        

        $category = DB::table('sub_categories')->where('id' , $id)->first();
        

        $products = DB::table('products')->where('subcategory' , $category->name)->get();

        //dd($products);

        $subcategory = DB::table('sub_categories')->where('name' , $category->name)->first();
        // dd($subcategory);
        
        $product_images = DB::table('product_images')->get();
       // dd($product_images);

        $pcategory = DB::table('parent_categories')->get();
        $sub_category  = DB::table('sub_categories')->get();
        //$this->data['category'] = $category;       
        //dd($products);         
        $system = DB::table('system')->first();
        
        return view('frontend.category')->with('products' , $products)
                                        ->with('pcategory' , $pcategory)
                                        ->with('sub_category' , $sub_category)
                                        ->with('product_images',$product_images)
                                        ->with('cat' , $subcategory)
                                        ->with('system' , $system);


    }

    
}

