<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Input;
use Hash;
use Validator;
use Auth;


class ProfileController extends BaseController
{




    public function show()
    {


        $system = DB::table('system')->first();
   		$pcategory = DB::table('parent_categories')->get();
        $sub_category  = DB::table('sub_categories')->get();
     

        return view('/frontend/profile', ['system' => $system , 'pcategory' => $pcategory, 'sub_category' => $sub_category ]);    }

}

