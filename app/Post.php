<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    
	protected $fillable = ['name' , 'image' , 'description' , 'slug' , 'created_by' , 'comment_count' , 'views'];

}